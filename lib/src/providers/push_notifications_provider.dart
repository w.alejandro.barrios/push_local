import 'dart:async';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationProvider {
  static int counter = 0;

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final _mensajesStreamController = StreamController<String>.broadcast();
  Stream<String> get mensajes => _mensajesStreamController.stream;

  initNotifications() async {
    _firebaseMessaging.requestNotificationPermissions();

    String token = await _firebaseMessaging.getToken();
    print("===== FCM Token ====");
    print(token);
    
    // ePlGVKig01s:APA91bH7AbFu1PhkoIb0xiwGQklXXeMYjX2Ddqp-bgSWCfZFDf3W1PDqpZwUVORLTsJu7xbo2_ok-UhBC0_xTSSYIYGkizCP1Tch6seAWkm3zvE4BQTLzgfe35-nUgWaCTb72SA3hlMg

    _firebaseMessaging.configure(
      // Cuando nuestra notificación está abierta
      onMessage: (info) async {
        if (counter%2==0) {
          print('====== ON Message =====');
          print(info);

          String argumento = 'no-data';
          if(Platform.isAndroid) {
            argumento = info['data']['comida'] ?? 'no-data';
          }

          _mensajesStreamController.sink.add(argumento);
        }
        counter++;
        
      },
      // Cuando la app está en segundo plano
      onLaunch: (info) async {
        if (counter%2==0) {
          print('====== ON onLaunch =====');
          print(info);
        }
        counter++;
      },

      onResume: (info) async {
        if (counter%2==0) {
          print('====== ON onResume =====');
          print(info);

          String argumento = 'no-data';
          if(Platform.isAndroid) {
            argumento = info['data']['comida'] ?? 'no-data';
          }

          _mensajesStreamController.sink.add(argumento);

        }
        counter++;
        //final notification = info['data']['comida'];
        //print(notification);
      }

    );
  }

  dispose() {
    _mensajesStreamController?.close();
  }

}